import 'package:alice_get_connect/alice_get_connect.dart';
import 'package:cadit_2/_core/constants/app_flavor.dart';
import 'package:cadit_2/data/_core/api_base_helper.dart';
import 'package:cadit_2/data/_core/interceptors/logger_interceptor.dart';
import 'package:cadit_2/data/current/current_data_binding.dart';
import 'package:cadit_2/data/forecast/forecast_data_binding.dart';
import 'package:get/get.dart';

class DataBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(
      ApiBaseHelper(
        client: GetConnect().httpClient,
        interceptors: [
          LoggerInterceptor(),
          if (AppFlavor.isDev) ...[
            Get.find<AliceGetConnect>(),
          ],
        ],
      ),
      permanent: true,
    );

    CurrentDataBinding().dependencies();
    ForecastDataBinding().dependencies();
  }
}
