import 'package:cadit_2/domain/forecast/entities/forecast.dart';

/// dt : 1706454000
/// main : {"temp":26.68,"feels_like":29.67,"temp_min":26.68,"temp_max":28.05,"pressure":1012,"sea_level":1012,"grnd_level":1010,"humidity":88,"temp_kf":-1.37}
/// weather : [{"id":500,"main":"Rain","description":"light rain","icon":"10n"}]
/// clouds : {"all":60}
/// wind : {"speed":1.43,"deg":290,"gust":2.99}
/// visibility : 10000
/// pop : 0.95
/// rain : {"3h":0.88}
/// sys : {"pod":"n"}
/// dt_txt : "2024-01-28 15:00:00"

class ForecastModel {
  ForecastModel({
    num? dt,
    Main? main,
    List<Weather>? weather,
    Clouds? clouds,
    Wind? wind,
    num? visibility,
    num? pop,
    Rain? rain,
    Sys? sys,
    String? dtTxt,
  }) {
    _dt = dt;
    _main = main;
    _weather = weather;
    _clouds = clouds;
    _wind = wind;
    _visibility = visibility;
    _pop = pop;
    _rain = rain;
    _sys = sys;
    _dtTxt = dtTxt;
  }

  ForecastModel.fromJson(dynamic json) {
    _dt = json['dt'];
    _main = json['main'] != null ? Main.fromJson(json['main']) : null;
    if (json['weather'] != null) {
      _weather = [];
      json['weather'].forEach((v) {
        _weather?.add(Weather.fromJson(v));
      });
    }
    _clouds = json['clouds'] != null ? Clouds.fromJson(json['clouds']) : null;
    _wind = json['wind'] != null ? Wind.fromJson(json['wind']) : null;
    _visibility = json['visibility'];
    _pop = json['pop'];
    _rain = json['rain'] != null ? Rain.fromJson(json['rain']) : null;
    _sys = json['sys'] != null ? Sys.fromJson(json['sys']) : null;
    _dtTxt = json['dt_txt'];
  }
  num? _dt;
  Main? _main;
  List<Weather>? _weather;
  Clouds? _clouds;
  Wind? _wind;
  num? _visibility;
  num? _pop;
  Rain? _rain;
  Sys? _sys;
  String? _dtTxt;
  ForecastModel copyWith({
    num? dt,
    Main? main,
    List<Weather>? weather,
    Clouds? clouds,
    Wind? wind,
    num? visibility,
    num? pop,
    Rain? rain,
    Sys? sys,
    String? dtTxt,
  }) =>
      ForecastModel(
        dt: dt ?? _dt,
        main: main ?? _main,
        weather: weather ?? _weather,
        clouds: clouds ?? _clouds,
        wind: wind ?? _wind,
        visibility: visibility ?? _visibility,
        pop: pop ?? _pop,
        rain: rain ?? _rain,
        sys: sys ?? _sys,
        dtTxt: dtTxt ?? _dtTxt,
      );
  num get dt => _dt ?? 0;
  Main get main => _main ?? Main();
  List<Weather> get weather => _weather ?? [];
  Clouds get clouds => _clouds ?? Clouds();
  Wind get wind => _wind ?? Wind();
  num get visibility => _visibility ?? 0;
  num get pop => _pop ?? 0;
  Rain get rain => _rain ?? Rain();
  Sys get sys => _sys ?? Sys();
  String get dtTxt => _dtTxt ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dt'] = _dt;
    if (_main != null) {
      map['main'] = _main?.toJson();
    }
    if (_weather != null) {
      map['weather'] = _weather?.map((v) => v.toJson()).toList();
    }
    if (_clouds != null) {
      map['clouds'] = _clouds?.toJson();
    }
    if (_wind != null) {
      map['wind'] = _wind?.toJson();
    }
    map['visibility'] = _visibility;
    map['pop'] = _pop;
    if (_rain != null) {
      map['rain'] = _rain?.toJson();
    }
    if (_sys != null) {
      map['sys'] = _sys?.toJson();
    }
    map['dt_txt'] = _dtTxt;
    return map;
  }

  Forecast toEntity() {
    return Forecast(
        dateTime: dtTxt.isNotEmpty ? DateTime.parse(dtTxt) : DateTime.now(),
        temp: main.temp,
        humidity: main.humidity);
  }
}

/// pod : "n"

class Sys {
  Sys({
    String? pod,
  }) {
    _pod = pod;
  }

  Sys.fromJson(dynamic json) {
    _pod = json['pod'];
  }
  String? _pod;
  Sys copyWith({
    String? pod,
  }) =>
      Sys(
        pod: pod ?? _pod,
      );
  String? get pod => _pod;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pod'] = _pod;
    return map;
  }
}

/// 3h : 0.88

class Rain {
  Rain({
    num? h,
  }) {
    _h = h;
  }

  Rain.fromJson(dynamic json) {
    _h = json['3h'];
  }
  num? _h;
  Rain copyWith({
    num? h,
  }) =>
      Rain(
        h: h ?? _h,
      );
  num? get h => _h;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['3h'] = _h;
    return map;
  }
}

/// speed : 1.43
/// deg : 290
/// gust : 2.99

class Wind {
  Wind({
    num? speed,
    num? deg,
    num? gust,
  }) {
    _speed = speed;
    _deg = deg;
    _gust = gust;
  }

  Wind.fromJson(dynamic json) {
    _speed = json['speed'];
    _deg = json['deg'];
    _gust = json['gust'];
  }
  num? _speed;
  num? _deg;
  num? _gust;
  Wind copyWith({
    num? speed,
    num? deg,
    num? gust,
  }) =>
      Wind(
        speed: speed ?? _speed,
        deg: deg ?? _deg,
        gust: gust ?? _gust,
      );
  num? get speed => _speed;
  num? get deg => _deg;
  num? get gust => _gust;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['speed'] = _speed;
    map['deg'] = _deg;
    map['gust'] = _gust;
    return map;
  }
}

/// all : 60

class Clouds {
  Clouds({
    num? all,
  }) {
    _all = all;
  }

  Clouds.fromJson(dynamic json) {
    _all = json['all'];
  }
  num? _all;
  Clouds copyWith({
    num? all,
  }) =>
      Clouds(
        all: all ?? _all,
      );
  num? get all => _all;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['all'] = _all;
    return map;
  }
}

/// id : 500
/// main : "Rain"
/// description : "light rain"
/// icon : "10n"

class Weather {
  Weather({
    num? id,
    String? main,
    String? description,
    String? icon,
  }) {
    _id = id;
    _main = main;
    _description = description;
    _icon = icon;
  }

  Weather.fromJson(dynamic json) {
    _id = json['id'];
    _main = json['main'];
    _description = json['description'];
    _icon = json['icon'];
  }
  num? _id;
  String? _main;
  String? _description;
  String? _icon;
  Weather copyWith({
    num? id,
    String? main,
    String? description,
    String? icon,
  }) =>
      Weather(
        id: id ?? _id,
        main: main ?? _main,
        description: description ?? _description,
        icon: icon ?? _icon,
      );
  num? get id => _id;
  String? get main => _main;
  String? get description => _description;
  String? get icon => _icon;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['main'] = _main;
    map['description'] = _description;
    map['icon'] = _icon;
    return map;
  }
}

/// temp : 26.68
/// feels_like : 29.67
/// temp_min : 26.68
/// temp_max : 28.05
/// pressure : 1012
/// sea_level : 1012
/// grnd_level : 1010
/// humidity : 88
/// temp_kf : -1.37

class Main {
  Main({
    num? temp,
    num? feelsLike,
    num? tempMin,
    num? tempMax,
    num? pressure,
    num? seaLevel,
    num? grndLevel,
    num? humidity,
    num? tempKf,
  }) {
    _temp = temp;
    _feelsLike = feelsLike;
    _tempMin = tempMin;
    _tempMax = tempMax;
    _pressure = pressure;
    _seaLevel = seaLevel;
    _grndLevel = grndLevel;
    _humidity = humidity;
    _tempKf = tempKf;
  }

  Main.fromJson(dynamic json) {
    _temp = json['temp'];
    _feelsLike = json['feels_like'];
    _tempMin = json['temp_min'];
    _tempMax = json['temp_max'];
    _pressure = json['pressure'];
    _seaLevel = json['sea_level'];
    _grndLevel = json['grnd_level'];
    _humidity = json['humidity'];
    _tempKf = json['temp_kf'];
  }
  num? _temp;
  num? _feelsLike;
  num? _tempMin;
  num? _tempMax;
  num? _pressure;
  num? _seaLevel;
  num? _grndLevel;
  num? _humidity;
  num? _tempKf;
  Main copyWith({
    num? temp,
    num? feelsLike,
    num? tempMin,
    num? tempMax,
    num? pressure,
    num? seaLevel,
    num? grndLevel,
    num? humidity,
    num? tempKf,
  }) =>
      Main(
        temp: temp ?? _temp,
        feelsLike: feelsLike ?? _feelsLike,
        tempMin: tempMin ?? _tempMin,
        tempMax: tempMax ?? _tempMax,
        pressure: pressure ?? _pressure,
        seaLevel: seaLevel ?? _seaLevel,
        grndLevel: grndLevel ?? _grndLevel,
        humidity: humidity ?? _humidity,
        tempKf: tempKf ?? _tempKf,
      );
  num get temp => _temp ?? 0;
  num get feelsLike => _feelsLike ?? 0;
  num get tempMin => _tempMin ?? 0;
  num get tempMax => _tempMax ?? 0;
  num get pressure => _pressure ?? 0;
  num get seaLevel => _seaLevel ?? 0;
  num get grndLevel => _grndLevel ?? 0;
  num get humidity => _humidity ?? 0;
  num get tempKf => _tempKf ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['temp'] = _temp;
    map['feels_like'] = _feelsLike;
    map['temp_min'] = _tempMin;
    map['temp_max'] = _tempMax;
    map['pressure'] = _pressure;
    map['sea_level'] = _seaLevel;
    map['grnd_level'] = _grndLevel;
    map['humidity'] = _humidity;
    map['temp_kf'] = _tempKf;
    return map;
  }
}
