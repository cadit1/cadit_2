import 'package:cadit_2/data/forecast/datasources/forecast_remote_datasource.dart';
import 'package:cadit_2/data/forecast/repositories/forecast_repository_impl.dart';
import 'package:cadit_2/domain/forecast/repositories/forecast_repository.dart';
import 'package:get/get.dart';

class ForecastDataBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForecastRemoteDatasource>(
        () => ForecastRemoteDatasourceImpl(Get.find()),
        fenix: true);

    Get.lazyPut<ForecastRepository>(() => ForecastRepositoryImpl(Get.find()),
        fenix: true);
  }
}
