import 'package:cadit_2/_core/utils/logger.dart';
import 'package:cadit_2/data/_core/api_base_helper.dart';
import 'package:cadit_2/data/_core/app_exceptions.dart';
import 'package:cadit_2/data/forecast/forecast_endpoints.dart';
import 'package:cadit_2/data/forecast/models/forecast_model.dart';

abstract class ForecastRemoteDatasource {
  Future<List<ForecastModel>> retrieveHourlyForecast();
}

class ForecastRemoteDatasourceImpl implements ForecastRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  ForecastRemoteDatasourceImpl(this.apiBaseHelper);
  @override
  Future<List<ForecastModel>> retrieveHourlyForecast() async {
    final responseBody =
        await apiBaseHelper.getApi(ForecastEndpoints.hourlyForecast, query: {
      'units': 'metric',
    });
    if (responseBody['cod'] != "200") {
      throw AppException.validation(responseBody['message']);
    }
    final List<dynamic> data = responseBody['list'] as List<dynamic>;
    final List<ForecastModel> forecastList = data
        .map((e) => ForecastModel.fromJson(e as Map<String, dynamic>))
        .toList();

    return forecastList;
  }
}
