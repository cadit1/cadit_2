import 'package:cadit_2/_core/extensions.dart';
import 'package:cadit_2/data/_core/app_exceptions.dart';
import 'package:cadit_2/data/forecast/datasources/forecast_remote_datasource.dart';
import 'package:cadit_2/domain/_core/app_failures.dart';
import 'package:cadit_2/domain/forecast/entities/forecast.dart';
import 'package:cadit_2/domain/forecast/repositories/forecast_repository.dart';
import 'package:dartz/dartz.dart';

class ForecastRepositoryImpl implements ForecastRepository {
  final ForecastRemoteDatasource forecastRemoteDatasource;

  ForecastRepositoryImpl(this.forecastRemoteDatasource);
  @override
  Future<Either<AppFailure, List<Forecast>>> retrieveHourlyForecast() async {
    try {
      final forecast = await forecastRemoteDatasource.retrieveHourlyForecast();
      return Right(forecast.map((e) => e.toEntity()).toList());
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }
}
