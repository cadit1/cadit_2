import 'package:cadit_2/_core/extensions.dart';
import 'package:cadit_2/data/_core/app_exceptions.dart';
import 'package:cadit_2/data/current/datasources/current_remote_datasource.dart';
import 'package:cadit_2/domain/_core/app_failures.dart';
import 'package:cadit_2/domain/current/entities/current.dart';
import 'package:cadit_2/domain/current/repositories/current_repository.dart';
import 'package:dartz/dartz.dart';

class CurrentRepositoryImpl implements CurrentRepository {
  final CurrentRemoteDatasource currentRemoteDatasource;

  CurrentRepositoryImpl(this.currentRemoteDatasource);

  @override
  Future<Either<AppFailure, Current>> retrieveCurrentWeather() async {
    try {
      final current = await currentRemoteDatasource.retrieveCurrentWeather();
      return Right(current.toEntity());
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }
}
