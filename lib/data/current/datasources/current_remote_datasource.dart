import 'package:cadit_2/_core/utils/logger.dart';
import 'package:cadit_2/data/_core/api_base_helper.dart';
import 'package:cadit_2/data/_core/app_exceptions.dart';
import 'package:cadit_2/data/_core/models/response_model.dart';
import 'package:cadit_2/data/current/current_endpoints.dart';
import 'package:cadit_2/data/current/models/current_model.dart';

abstract class CurrentRemoteDatasource {
  Future<CurrentModel> retrieveCurrentWeather();
}

class CurrentRemoteDatasourceImpl implements CurrentRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  CurrentRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<CurrentModel> retrieveCurrentWeather() async {
    final responseBody =
        await apiBaseHelper.getApi(CurrentEndpoints.currentWeather, query: {
      'units': 'metric',
    });
    if (responseBody['cod'] != 200) {
      throw AppException.validation(responseBody['message']);
    }
    return CurrentModel.fromJson(responseBody);
  }
}
