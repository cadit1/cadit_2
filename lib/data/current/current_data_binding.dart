import 'package:cadit_2/data/current/datasources/current_remote_datasource.dart';
import 'package:cadit_2/data/current/repositories/current_repository_impl.dart';
import 'package:cadit_2/domain/current/repositories/current_repository.dart';
import 'package:get/get.dart';

class CurrentDataBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CurrentRemoteDatasource>(
        () => CurrentRemoteDatasourceImpl(Get.find()),
        fenix: true);

    Get.lazyPut<CurrentRepository>(() => CurrentRepositoryImpl(Get.find()),
        fenix: true);
  }
}
