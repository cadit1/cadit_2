import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:cadit_2/_core/constants/app_flavor.dart';
import 'package:cadit_2/_core/my_app.dart';

void main() async {
  await initializeDateFormatting("id_ID", null);
  Intl.defaultLocale = 'id_ID';
  WidgetsFlutterBinding.ensureInitialized();
  AppFlavor(
    baseUrl: 'https://api.openweathermap.org',
    flavor: Flavor.dev,
  );
  runApp(const MyApp());
}
