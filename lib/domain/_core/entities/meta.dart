class Meta {
  final int count;
  final int total;
  final int perPage;
  final int currentPage;
  final int totalPage;
  final bool hasNext;

  const Meta({
    this.count = 0,
    this.total = 0,
    this.perPage = 0,
    this.currentPage = 1,
    this.totalPage = 1,
    this.hasNext = false,
  });

  Meta copyWith({
    int? count,
    int? total,
    int? perPage,
    int? currentPage,
    int? totalPage,
    bool? hasNext,
  }) =>
      Meta(
        count: count ?? this.count,
        total: total ?? this.total,
        perPage: perPage ?? this.perPage,
        currentPage: currentPage ?? this.currentPage,
        totalPage: totalPage ?? this.totalPage,
        hasNext: hasNext ?? this.hasNext,
      );
}
