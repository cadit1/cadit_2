class Current {
  final String name;
  final num temp;
  final String status;
  final num feelsLike;
  final num humidity;
  final num pressure;
  final num windSpeed;
  final num cloud;
  final String iconCode;

  Current({
    this.name = '',
    this.temp = 0,
    this.status = '',
    this.feelsLike = 0,
    this.humidity = 0,
    this.pressure = 0,
    this.windSpeed = 0,
    this.cloud = 0,
    this.iconCode = '',
  });
}
