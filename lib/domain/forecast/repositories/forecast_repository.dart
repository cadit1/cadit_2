import 'package:cadit_2/domain/_core/app_failures.dart';
import 'package:cadit_2/domain/forecast/entities/forecast.dart';
import 'package:dartz/dartz.dart';

abstract class ForecastRepository {
  Future<Either<AppFailure, List<Forecast>>> retrieveHourlyForecast();
}
