class Forecast {
  final DateTime? dateTime;
  final num temp;
  final num humidity;

  Forecast({this.dateTime, this.temp = 0, this.humidity = 0});
}
