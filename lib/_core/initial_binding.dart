import 'package:get/get.dart';
import 'package:cadit_2/data/data_binding.dart';
import 'package:cadit_2/domain/domain_binding.dart';
import 'package:cadit_2/presentation/presentation_binding.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    DataBinding().dependencies();
    DomainBinding().dependencies();
    PresentationBinding().dependencies();
  }
}
