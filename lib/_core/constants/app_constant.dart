class AppConstant {
  static const kAppName = 'CAD IT Weather';
  static const kToken = "TOKEN";
  static const kUser = "USER";
  static const kUsername = "USERNAME";
  static const kParsingDataCode = "parsing_data_exception";
  static const kConnectionError = "connection_timeout";
  static const kStaticToken = "ac30e02de1b53a97325276f2a4c6a632";

  static const kConnectionTimeout = 30000;
  static const kDefaultPadding = 16.0;
}
