import 'package:cadit_2/presentation/screens/home/home_binding.dart';
import 'package:cadit_2/presentation/screens/home/screens/home_screen.dart';
import 'package:get/get.dart';

class AppPage {
  static const homeScreen = '/';

  static var pages = <GetPage>[
    //SPLASH
    GetPage(
      name: homeScreen,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
  ];
}
