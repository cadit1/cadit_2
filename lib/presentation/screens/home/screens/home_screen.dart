import 'package:cached_network_image/cached_network_image.dart';
import 'package:cadit_2/_core/constants/app_constant.dart';
import 'package:cadit_2/presentation/_core/app_color.dart';
import 'package:cadit_2/presentation/_core/app_text_style.dart';
import 'package:cadit_2/presentation/_core/widgets/custom_shimmer.dart';
import 'package:cadit_2/presentation/screens/home/controllers/home_controller.dart';
import 'package:cadit_2/presentation/screens/home/widgets/hourly_temperature_chart.dart';
import 'package:cadit_2/presentation/screens/home/widgets/main_info_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_icons/weather_icons.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});
  final _controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.celestialBlue,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Obx(() {
              if (_controller.isLoading.isFalse) {
                return Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppConstant.kDefaultPadding,
                      vertical: AppConstant.kDefaultPadding / 2 * 3),
                  color: AppColor.celestialBlue,
                  width: double.infinity,
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: '${_controller.currentWeather.value.name} ',
                        style: AppTextStyle.headlineMedium.copyWith(
                            color: AppColor.neutral1,
                            fontWeight: FontWeight.bold),
                      ),
                      const WidgetSpan(
                        child: Icon(
                          Icons.location_pin,
                          color: AppColor.neutral1,
                        ),
                      )
                    ]),
                  ),
                );
              }
              return Container(
                padding: const EdgeInsets.symmetric(
                    horizontal: AppConstant.kDefaultPadding,
                    vertical: AppConstant.kDefaultPadding / 2 * 3),
                child: CustomShimmer(
                    child: TextPlaceHolder(
                  height: 20,
                  width: MediaQuery.of(context).size.width / 2,
                )),
              );
            }),
            Expanded(
              child: RefreshIndicator(
                color: AppColor.celestialBlue,
                onRefresh: () {
                  _controller.onRefresh();
                  return Future(() => true);
                },
                child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppConstant.kDefaultPadding,
                      vertical: AppConstant.kDefaultPadding),
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(() {
                        if (_controller.isLoading.isFalse) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      '${_controller.currentWeather.value.temp.toInt()}°',
                                      style: AppTextStyle.displayLarge
                                          .copyWith(color: AppColor.neutral1),
                                    ),
                                    const SizedBox(
                                      height: AppConstant.kDefaultPadding / 2,
                                    ),
                                    Text(
                                      _controller.currentWeather.value.status
                                          .capitalize!,
                                      style: AppTextStyle.titleMedium
                                          .copyWith(color: AppColor.neutral1),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: CachedNetworkImage(
                                  imageUrl:
                                      'https://openweathermap.org/img/wn/${_controller.currentWeather.value.iconCode}@2x.png',
                                  height: 70,
                                  errorWidget: (ctx, _, __) {
                                    return Container();
                                  },
                                ),
                              )
                            ],
                          );
                        }
                        return const CustomShimmer(
                          child: TextPlaceHolder(
                            height: 100,
                          ),
                        );
                      }),
                      const SizedBox(
                        height: AppConstant.kDefaultPadding * 2,
                      ),
                      Obx(() {
                        if (_controller.isLoading.isFalse) {
                          return Text(
                            '${_controller.currentWeather.value.temp}° Feels like ${_controller.currentWeather.value.feelsLike}°',
                            style: AppTextStyle.titleMedium
                                .copyWith(color: AppColor.neutral1),
                          );
                        }
                        return CustomShimmer(
                            child: TextPlaceHolder(
                          width: MediaQuery.of(context).size.width / 2,
                        ));
                      }),
                      const SizedBox(
                        height: AppConstant.kDefaultPadding * 2,
                      ),
                      Obx(() {
                        if (_controller.isLoading.isFalse) {
                          return Container(
                            width: double.infinity,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color:
                                  AppColor.mediumPersianBlue.withOpacity(0.2),
                            ),
                            child: SingleChildScrollView(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: AppConstant.kDefaultPadding / 2,
                                  vertical: AppConstant.kDefaultPadding / 2),
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: [
                                  MainInfoItem(
                                    icon: WeatherIcons.humidity,
                                    value:
                                        '${_controller.currentWeather.value.humidity}%',
                                    title: 'Humidity',
                                  ),
                                  const SizedBox(
                                    width: AppConstant.kDefaultPadding,
                                  ),
                                  MainInfoItem(
                                    icon: WeatherIcons.cloudy,
                                    value:
                                        '${_controller.currentWeather.value.cloud}%',
                                    title: 'Cloudiness',
                                  ),
                                  const SizedBox(
                                    width: AppConstant.kDefaultPadding,
                                  ),
                                  MainInfoItem(
                                    icon: WeatherIcons.barometer,
                                    value:
                                        '${_controller.currentWeather.value.pressure} hpa',
                                    title: 'Pressure',
                                  ),
                                  const SizedBox(
                                    width: AppConstant.kDefaultPadding,
                                  ),
                                  MainInfoItem(
                                    icon: WeatherIcons.wind,
                                    value:
                                        '${_controller.currentWeather.value.windSpeed} m/s',
                                    title: 'Wind',
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                        return const CustomShimmer(
                            child: TextPlaceHolder(
                          height: 120,
                        ));
                      }),
                      const SizedBox(
                        height: AppConstant.kDefaultPadding,
                      ),
                      Obx(() {
                        if (_controller.isLoading.isFalse) {
                          return Container(
                            width: double.infinity,
                            height: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color:
                                  AppColor.mediumPersianBlue.withOpacity(0.2),
                            ),
                            child: SingleChildScrollView(
                              physics: const BouncingScrollPhysics(
                                  decelerationRate:
                                      ScrollDecelerationRate.fast),
                              padding: const EdgeInsets.symmetric(
                                  vertical: AppConstant.kDefaultPadding / 2),
                              scrollDirection: Axis.horizontal,
                              child: HourlyTemperatureChart(
                                  forecasts: _controller.hourlyForecast),
                            ),
                          );
                        }
                        return const CustomShimmer(
                            child: TextPlaceHolder(
                          height: 200,
                        ));
                      }),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
