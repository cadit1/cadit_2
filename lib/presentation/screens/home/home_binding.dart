import 'package:cadit_2/presentation/screens/home/controllers/home_controller.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController(Get.find(), Get.find()));
  }
}
