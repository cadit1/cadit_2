import 'package:cadit_2/_core/utils/logger.dart';
import 'package:cadit_2/domain/current/entities/current.dart';
import 'package:cadit_2/domain/current/repositories/current_repository.dart';
import 'package:cadit_2/domain/forecast/entities/forecast.dart';
import 'package:cadit_2/domain/forecast/repositories/forecast_repository.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:location/location.dart' as l;

enum LocationSection {
  initial,
  noLocationPermission,
  noLocationPermissionPermanent,
  useLocation,
}

class HomeController extends GetxController {
  final CurrentRepository currentRepository;
  final ForecastRepository forecastRepository;

  HomeController(this.currentRepository, this.forecastRepository);

  final currentWeather = Current().obs;
  final hourlyForecast = <Forecast>[].obs;
  final locationSection = LocationSection.initial.obs;
  l.Location location = l.Location();
  final isLocationEnabled = true.obs;
  final isLoading = false.obs;

  @override
  void onInit() async {
    super.onInit();
    await _checkPermission();
  }

  Future<void> retrieveCurrentWeather() async {
    final retrieve = await currentRepository.retrieveCurrentWeather();
    retrieve.fold((failure) {}, (data) {
      currentWeather.value = data;
    });
  }

  Future<void> retrieveHourlyForecast() async {
    final retrieve = await forecastRepository.retrieveHourlyForecast();
    retrieve.fold((failure) {}, (data) {
      hourlyForecast.value = data;
    });
  }

  Future<void> _checkPermission() async {
    isLoading(true);
    if (await _requestLocationPermissions()) {
      await retrieveAllData();
    }
  }

  Future<void> retrieveAllData() async {
    isLoading(true);
    await retrieveCurrentWeather();
    await retrieveHourlyForecast();
    isLoading(false);
  }

  onRefresh() async {
    await retrieveAllData();
  }

  Future<bool> _requestLocationPermissions() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      logger.e(permission);
      if (permission == LocationPermission.denied) {
        locationSection.value = LocationSection.noLocationPermission;
        return false;
      }
      if (permission == LocationPermission.deniedForever) {
        locationSection.value = LocationSection.noLocationPermissionPermanent;
        return false;
      }
    } else if (permission == LocationPermission.whileInUse ||
        permission == LocationPermission.always) {
      locationSection.value = LocationSection.useLocation;
    }
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
    }
    isLocationEnabled(serviceEnabled);
    return serviceEnabled;
  }
}
