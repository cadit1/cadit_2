import 'package:cadit_2/presentation/_core/app_color.dart';
import 'package:cadit_2/presentation/_core/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';

class MainInfoItem extends StatelessWidget {
  const MainInfoItem({
    super.key,
    required this.icon,
    required this.value,
    required this.title,
  });

  final IconData icon;
  final String value;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BoxedIcon(
          size: 40,
          icon,
          color: AppColor.neutral1,
        ),
        Text(
          value,
          style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
        ),
        Text(
          title,
          style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
        )
      ],
    );
  }
}
