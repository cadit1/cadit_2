import 'package:cadit_2/_core/constants/app_constant.dart';
import 'package:cadit_2/domain/forecast/entities/forecast.dart';
import 'package:cadit_2/presentation/_core/app_color.dart';
import 'package:cadit_2/presentation/_core/app_text_style.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HourlyTemperatureChart extends StatelessWidget {
  final List<Forecast> forecasts;

  const HourlyTemperatureChart({Key? key, required this.forecasts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 3000,
      child: BarChart(
        BarChartData(
          alignment: BarChartAlignment.spaceAround,
          barTouchData: BarTouchData(enabled: false),
          borderData: FlBorderData(show: false),
          gridData: const FlGridData(show: false),
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 48,
                getTitlesWidget: (value, _) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        bottom: AppConstant.kDefaultPadding / 2),
                    child: Column(
                      children: [
                        Text(
                          DateFormat('h a')
                              .format(forecasts[value.toInt()].dateTime!),
                          style: AppTextStyle.bodyMedium
                              .copyWith(color: AppColor.neutral5),
                        ),
                        Text(
                          '${forecasts[value.toInt()].temp.toInt()}°C',
                          style: AppTextStyle.bodyMedium
                              .copyWith(color: AppColor.neutral1),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            leftTitles:
                const AxisTitles(sideTitles: SideTitles(showTitles: false)),
            rightTitles:
                const AxisTitles(sideTitles: SideTitles(showTitles: false)),
            bottomTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 28,
                getTitlesWidget: (value, _) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: AppConstant.kDefaultPadding / 2),
                    child: Column(
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              const WidgetSpan(
                                child: Icon(
                                  Icons.water_drop,
                                  color: AppColor.neutral5,
                                  size: 16,
                                ),
                              ),
                              TextSpan(
                                  text: '${forecasts[value.toInt()].humidity}%',
                                  style: AppTextStyle.bodyMedium
                                      .copyWith(color: AppColor.neutral5)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            show: true,
          ),
          barGroups: forecasts.asMap().entries.map((e) {
            return BarChartGroupData(x: e.key.toInt(), barRods: [
              BarChartRodData(
                  toY: e.value.temp.toDouble(), color: AppColor.neutral1)
            ]);
          }).toList(),
        ),
      ),
    );
  }
}
