import 'package:cadit_2/presentation/screens/home/home_binding.dart';
import 'package:get/get.dart';

class PresentationBinding extends Bindings {
  @override
  void dependencies() {
    HomeBinding().dependencies();
  }
}
