class AppIcon {
  static const icEye = 'assets/icons/ic_eye.png';
  static const icEyeFilled = 'assets/icons/ic_eye_filled.png';
  static const icLockFilled = 'assets/icons/ic_lock_filled.png';
  static const icLock = 'assets/icons/ic_lock.png';
  static const icUser = 'assets/icons/ic_user.png';
  static const icUserSquare = 'assets/icons/ic_user_square.png';
  static const icCloseSmall = 'assets/icons/ic_close_small.png';
  static const icCloseMedium = 'assets/icons/ic_close_medium.png';
  static const icCloseLarge = 'assets/icons/ic_close_large.png';
  static const icClose24 = 'assets/icons/ic_close_24.png';
  static const icAddCircle = 'assets/icons/ic_add_circle.png';
  static const icLogoEPresensi = 'assets/icons/ic_logo_e_presensi.png';
  static const icLogoutFilled = 'assets/icons/ic_logout_filled.png';
  static const icLogout = 'assets/icons/ic_logout.png';
  static const icNotification = 'assets/icons/ic_notification.png';
  static const icLogin = 'assets/icons/ic_login.png';
  static const icLoginFilled = 'assets/icons/ic_login_filled.png';
  static const icLawStamp16 = 'assets/icons/ic_law_stamp_16.png';
  static const icLawStamp20 = 'assets/icons/ic_law_stamp_20.png';
  static const icLawStamp24 = 'assets/icons/ic_law_stamp_24.png';
  static const icCalendar16 = 'assets/icons/ic_calendar_16.png';
  static const icCalendar16Filled = 'assets/icons/ic_calendar_16_filled.png';
  static const icCalendar24 = 'assets/icons/ic_calendar_24.png';
  static const icCalendarHalf20 = 'assets/icons/ic_calendar_half_20.png';
  static const icClock = 'assets/icons/ic_clock.png';
  static const icClock20 = 'assets/icons/ic_clock_20.png';
  static const icArrowCircleRight = 'assets/icons/ic_arrow_circle_right.png';
  static const icTaskSquare = 'assets/icons/ic_task_square.png';
  static const icLocation = 'assets/icons/ic_location.png';
  static const icFilter = 'assets/icons/ic_filter.png';
  static const icSearch = 'assets/icons/ic_search.png';
  static const icArrowLeft = 'assets/icons/ic_arrow_left.png';
  static const icArrowRight = 'assets/icons/ic_arrow_right.png';
  static const icArrowUp = 'assets/icons/ic_arrow_up.png';
  static const icArrowDown = 'assets/icons/ic_arrow_down.png';
  static const icNoImage40 = 'assets/icons/ic_no_image_40.png';
  static const icNoImage16 = 'assets/icons/ic_no_image_16.png';
  static const icImage16 = 'assets/icons/ic_image_16.png';
  static const icImage24 = 'assets/icons/ic_image_24.png';
  static const icDocument = 'assets/icons/ic_document.png';
  static const icInformation16 = 'assets/icons/ic_information_16.png';
  static const icInformationOutline16 =
      'assets/icons/ic_information_outline_16.png';
  static const icInformationCircle16 =
      'assets/icons/ic_information_circle_16.png';
  static const icReceipt = 'assets/icons/ic_receipt.png';
  static const icEdit20 = 'assets/icons/ic_edit_20.png';
  static const icEdit16 = 'assets/icons/ic_edit_16.png';
  static const icTrash = 'assets/icons/ic_trash.png';
  static const icSetting = 'assets/icons/ic_setting.png';
  static const icSms = 'assets/icons/ic_sms.png';
}

class AppImage {
  static const imgBbwsCitarum = 'assets/images/img_bbws_citarum.png';
  static const imgNotFound = 'assets/images/img_not_found.png';
  static const imgEmptyState = 'assets/images/img_empty_state.png';
  static const imgOnboarding = 'assets/images/img_onboarding.png';
}
